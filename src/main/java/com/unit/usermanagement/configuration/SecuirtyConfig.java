package com.unit.usermanagement.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecuirtyConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private AuthEntryPointJwt entryPointJwt;

    @Override
    protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
        authBuilder.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AuthTokenFilter createAuthTokenFilter() {
        return new AuthTokenFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .authorizeRequests()
                .antMatchers("/api/v1/accounts/management/**").hasAnyAuthority("ADMIN")
                .antMatchers("/api/v1/auth/**").permitAll()
                .antMatchers("/mvc/auth/**").permitAll()
                .antMatchers("/mvc/admin/**").hasAuthority("ADMIN")
                .antMatchers("/css/**").permitAll()
                .antMatchers("**/css/**").permitAll()
                .antMatchers("/webjars/bootstrap/4.3.1/css/bootstrap.min.css").permitAll()
                .antMatchers("/v2/api-docs", "/webjars/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(entryPointJwt)
                .and()
                .httpBasic()
                .and()
                .csrf().disable();

        http.addFilterBefore(createAuthTokenFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}