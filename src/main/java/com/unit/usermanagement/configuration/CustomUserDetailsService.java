package com.unit.usermanagement.configuration;

import com.unit.usermanagement.dto.RoleDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.repository.AccountRepository;
import com.unit.usermanagement.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Transactional(transactionManager = "transactionManager")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);
        if (account == null) throw new UsernameNotFoundException(username);

        List<RoleDTO> roleDTOList = roleRepository.getByUsername(account.getUsername());
        if (roleDTOList.size() == 0) {
            return new User(account.getUsername(), account.getPassword(), AuthorityUtils.createAuthorityList("USER"));

        } else {
//        Cách 1
//        List<String> roleList=new ArrayList<>();
//        for (RoleDTO role:roleDTOList) {
//            roleList.add(role.getRole());
//        }
//        return new User(account.getUsername(), account.getPassword(), AuthorityUtils.createAuthorityList(roleList.toArray(new String[0])));

//        Cách 2
            List<GrantedAuthority> authorities = roleDTOList.stream()
                    .map(roleDTO -> new SimpleGrantedAuthority(roleDTO.getRole()))
                    .collect(Collectors.toList());

            return new User(account.getUsername(), account.getPassword(), authorities);
        }
    }

    public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(email);
        if (account == null) {
            throw new UsernameNotFoundException(email);
        } else {
            return new User(account.getUsername(), account.getPassword(), AuthorityUtils.createAuthorityList("USER"));
        }
    }
}
