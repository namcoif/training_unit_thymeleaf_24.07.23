package com.unit.usermanagement.configuration;

import com.miragesql.miragesql.SqlManagerImpl;
import com.miragesql.miragesql.bean.BeanDescFactory;
import com.miragesql.miragesql.bean.FieldPropertyExtractor;
import com.miragesql.miragesql.dialect.SQLServerDialect;
import com.miragesql.miragesql.integration.spring.SpringConnectionProvider;
import com.miragesql.miragesql.naming.RailsLikeNameConverter;
import com.miragesql.miragesql.provider.ConnectionProvider;
import jp.xet.springframework.data.mirage.repository.config.EnableMirageRepositories;
import jp.xet.springframework.data.mirage.repository.support.MiragePersistenceExceptionTranslator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

@Configuration
@EnableMirageRepositories(basePackages = "com.unit.usermanagement.repository", sqlManagerRef = "sqlManager")
public class MirageConfig {

    @Bean("sqlManager")
    public SqlManagerImpl sqlManager(DataSourceTransactionManager transactionManager) {
        SqlManagerImpl sqlManagerImpl = new SqlManagerImpl();
        sqlManagerImpl.setConnectionProvider(connectionProvider(transactionManager));
        sqlManagerImpl.setDialect(new SQLServerDialect());
        sqlManagerImpl.setBeanDescFactory(beanDescFactory());
        sqlManagerImpl.setNameConverter(new RailsLikeNameConverter());
        return sqlManagerImpl;
    }

    @Bean
    public ConnectionProvider connectionProvider(
            DataSourceTransactionManager transactionManager) {
        SpringConnectionProvider springConnectionProvider = new SpringConnectionProvider();
        springConnectionProvider.setTransactionManager(transactionManager);
        return springConnectionProvider;
    }

    @Bean
    public BeanDescFactory beanDescFactory() {
        BeanDescFactory beanDescFactory = new BeanDescFactory();
        beanDescFactory.setPropertyExtractor(new FieldPropertyExtractor());
        return beanDescFactory;
    }

    @Bean
    public MiragePersistenceExceptionTranslator persistenceExceptionTranslator() {
        return new MiragePersistenceExceptionTranslator();
    }
}