package com.unit.usermanagement.configuration;

import com.unit.usermanagement.exception.StringValidator;
import com.unit.usermanagement.utils.TextUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ComponentConfiguration {

    @Value("google.api-key")
    private String apiKey;

    @Bean
    public ModelMapper initModelMapper() {
        return new ModelMapper();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public StringValidator stringValidator() {
        return new StringValidator();
    }

    @Bean
    public TextUtils initUtils() {
        return new TextUtils(apiKey);
    }
}
