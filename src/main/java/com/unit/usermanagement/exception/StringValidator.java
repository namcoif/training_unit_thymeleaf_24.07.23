package com.unit.usermanagement.exception;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator {

    public static boolean isSpecialCharacters(String input) {
        String regex = "^[a-zA-Z0-9_]*$";
        Pattern pattern = Pattern.compile(regex);
        if (input != null) {
            Matcher matcher = pattern.matcher(input);
            return matcher.matches();
        }
        return false;
    }

}
