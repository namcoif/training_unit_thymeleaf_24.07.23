package com.unit.usermanagement.thymeleafcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mvc/home")
public class IndexThymeController {
    @GetMapping("/index")
    public String indexPage(Model model) {
        String message = "Test";
        model.addAttribute("test", message);
        return "/index";
    }
}
