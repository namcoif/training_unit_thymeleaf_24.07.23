package com.unit.usermanagement.thymeleafcontroller;

import com.unit.usermanagement.dto.AccountDTO;
import com.unit.usermanagement.dto.QueryDTO;
import com.unit.usermanagement.dto.RegisterDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.exception.StringValidator;
import com.unit.usermanagement.service.IAccountService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(value = "/mvc/admin")
public class MasterThymeController {

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    private Validator validator;
    private static Integer pageNumber = 0;
    private static final Integer pageSize = 10;
    private static final List<String> GENDER_LIST = Arrays.asList("Male", "Female", "Other");
    private static final List<String> TYPE_LIST = Arrays.asList("ADMIN", "USER");
    private static Page<AccountDTO> accountPage = null;
    private static Sort sort = null;
    private static Integer totalPages = 0;

    public MasterThymeController() {
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management")
    public String accountGetAll(Model model, RedirectAttributes redirectAttributes) {
        QueryDTO queryDTO = new QueryDTO();
        try {
            Iterable<Account> accounts = accountService.accountGetAll();
            List<AccountDTO> accountDTOS = modelMapper.map(accounts, new TypeToken<List<AccountDTO>>() {
            }.getType());

            model.addAttribute("account", new Account());
            model.addAttribute("accountList", accountDTOS);

            model.addAttribute("gendersList", GENDER_LIST);
            model.addAttribute("typesList", TYPE_LIST);

            model.addAttribute("queryDTO", queryDTO);

            return "redirect:/mvc/admin/account-management/paging";

//            return "master/account-management";

        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());

            return "redirect:/mvc/admin/account-management/paging";

//            return "master/account-management";
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management/paging")
    public String accountGetByPage(@ModelAttribute("success") String success, @ModelAttribute("error") String error, @ModelAttribute("status") String status, Model model, Pageable pageable, @PathParam("keySearch") QueryDTO queryDTO) {
        try {
            sort = pageable.getSort();
            if (pageable.getPageNumber() > 0) {
                pageNumber = pageable.getPageNumber() - 1;
            }
            pageable = PageRequest.of(pageNumber, pageSize, sort);
            accountPage = accountService.accountGetByPage(pageable, queryDTO);

            model.addAttribute("account", new Account());
            model.addAttribute("accountList", accountPage);
            model.addAttribute("pageData", accountPage);

            model.addAttribute("gendersList", GENDER_LIST);
            model.addAttribute("typesList", TYPE_LIST);

            model.addAttribute("queryDTO", queryDTO);

            totalPages = accountPage.getTotalPages();
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("error", error);
            model.addAttribute("success", success);
            model.addAttribute("status", status);

            return "master/account-management";

        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());

            return "redirect:/mvc/admin/account-management/paging";

//            return "master/account-management";

        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/account-management/paging")
    public String accountSearchByPage(Pageable pageable, @ModelAttribute QueryDTO queryDTO, Model model) {

        sort = pageable.getSort();
//        if (pageable.getPageNumber() > 0) {
//            pageNumber = pageable.getPageNumber() - 1;
//        }
        pageable = PageRequest.of(0, pageSize, sort);
        accountPage = accountService.accountGetByPage(pageable, queryDTO);

        model.addAttribute("account", new Account());
        model.addAttribute("accountList", accountPage);
        model.addAttribute("pageData", accountPage);

        model.addAttribute("gendersList", GENDER_LIST);
        model.addAttribute("typesList", TYPE_LIST);

        model.addAttribute("queryDTO", queryDTO);

        totalPages = accountPage.getTotalPages();
        model.addAttribute("totalPages", totalPages);

        return "master/account-management";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management/{id}")
    public String accountGetById(@PathVariable("id") Long id, @RequestParam(defaultValue = "get") String typeGet, @PathParam("search") QueryDTO queryDTO, Model model, Pageable pageable, HttpSession session) {
        try {
            Account account = accountService.getById(id);

            RegisterDTO acccountModified = modelMapper.map(account, RegisterDTO.class);

            if (queryDTO == null) {
                queryDTO = new QueryDTO();
            }
            model.addAttribute("account", acccountModified);
            session.setAttribute("account", acccountModified);


            sort = pageable.getSort();
            if (pageable.getPageNumber() > 0) {
                pageNumber = pageable.getPageNumber() - 1;
            }
            pageable = PageRequest.of(pageNumber, pageSize, sort);
            accountPage = accountService.accountGetByPage(pageable, queryDTO);

            model.addAttribute("accountList", accountPage);
            model.addAttribute("pageData", accountPage);

            model.addAttribute("gendersList", GENDER_LIST);
            model.addAttribute("typesList", TYPE_LIST);

            model.addAttribute("queryDTO", queryDTO);

            totalPages = accountPage.getTotalPages();
            model.addAttribute("totalPages", totalPages);

            model.addAttribute("typeGet", typeGet);
//            return "redirect:/mvc/admin/account-management";
            return "master/account-management";


        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "master/account-management";
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management/delete/{id}")
    public String accountDeleteById(@PathVariable("id") Long id, @PathParam("search") QueryDTO queryDTO, RedirectAttributes model, Pageable pageable, HttpSession session) {
        Account accountLogined = (Account) session.getAttribute("accountLogined");
        try {
            boolean isDeleted = false;
            if (accountLogined != null) {
                isDeleted = accountService.deleteById(id, accountLogined.getId());
            } else {
                isDeleted = accountService.deleteById(id, null);
            }
            if (isDeleted) {
//                model.addAttribute("account", new RegisterDTO());
//
//                sort = pageable.getSort();
//                if (pageable.getPageNumber() > 0) {
//                    pageNumber = pageable.getPageNumber() - 1;
//                }
//                pageable = PageRequest.of(pageNumber, pageSize, sort);
//                accountPage = accountService.accountGetByPage(pageable, queryDTO);
//
//                model.addAttribute("accountList", accountPage);
//                model.addAttribute("pageData", accountPage);
//
//                model.addAttribute("gendersList", GENDER_LIST);
//                model.addAttribute("typesList", TYPE_LIST);
//
//                model.addAttribute("queryDTO", queryDTO);
//
//                totalPages = accountPage.getTotalPages();
//                model.addAttribute("totalPages", totalPages);
                model.addFlashAttribute("success", "Account deleted successfully!");
                return "redirect:/mvc/admin/account-management/paging";
            } else {
//                model.addAttribute("account", new RegisterDTO());
//
//                sort = pageable.getSort();
//                if (pageable.getPageNumber() > 0) {
//                    pageNumber = pageable.getPageNumber() - 1;
//                }
//                pageable = PageRequest.of(pageNumber, pageSize, sort);
//                accountPage = accountService.accountGetByPage(pageable, queryDTO);
//
//                model.addAttribute("accountList", accountPage);
//                model.addAttribute("pageData", accountPage);
//
//                model.addAttribute("gendersList", GENDER_LIST);
//                model.addAttribute("typesList", TYPE_LIST);
//
//                model.addAttribute("queryDTO", queryDTO);
//
//                totalPages = accountPage.getTotalPages();
//                model.addAttribute("totalPages", totalPages);
                model.addAttribute("error", "Can't delete the person who is logged in!");
                return "redirect:/mvc/admin/account-management/paging";
            }


        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "master/account-management";
        }
    }


    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/account-management")
    public String saveAccount(HttpServletRequest request, @ModelAttribute RegisterDTO registerDTO, Pageable pageable, Model model, HttpSession session) {
        String clientIP = (String) request.getAttribute("clientIP");

//Check Validations

        boolean isInvalid = false;
        String errorUsername = "";
        String errorPassword = "";
        if (!StringValidator.isSpecialCharacters(registerDTO.getUsername())) {
            isInvalid = true;
            errorUsername += "The username must not contain special characters!\n";
        }
        if ("".equals(registerDTO.getUsername()) || registerDTO.getUsername() == null) {
            isInvalid = true;
            errorUsername += "The username must not is blank!\n";
        }
        if (registerDTO.getPassword().length() < 6) {
            isInvalid = true;
            errorPassword += "\nPassword must be 6 or more characters!\n";
        }
//Invalid
        if (isInvalid) {
            sort = pageable.getSort();
            if (pageable.getPageNumber() > 0) {
                pageNumber = pageable.getPageNumber() - 1;
            }
            pageable = PageRequest.of(pageNumber, pageSize, sort);

            accountPage = accountService.accountGetByPage(pageable, new QueryDTO());

            model.addAttribute("accountList", accountPage);
            model.addAttribute("pageData", accountPage);

            model.addAttribute("errorUsername", errorUsername);
            model.addAttribute("errorPassword", errorPassword);

            model.addAttribute("typeGet", "get");
            model.addAttribute("account", registerDTO);

            model.addAttribute("gendersList", GENDER_LIST);
            model.addAttribute("typesList", TYPE_LIST);

            model.addAttribute("queryDTO", new QueryDTO());

            totalPages = accountPage.getTotalPages();
            model.addAttribute("totalPages", totalPages);
//            return "redirect:/mvc/admin/account-management/paging";

            return "master/account-management";
        }
//Valid
        try {
            Account accountSave = modelMapper.map(registerDTO, Account.class);
            RegisterDTO accountModifying = (RegisterDTO) session.getAttribute("account");
            Account accountLogined = (Account) session.getAttribute("accountLogined");

            String status = accountService.saveAccount(accountSave, accountModifying, accountLogined);

            if ("updated".equalsIgnoreCase(status)) {
                model.addAttribute("updated", "Account updated successfully!");

                registerDTO = new RegisterDTO();
                session.setAttribute("account", registerDTO);
                model.addAttribute("account", registerDTO);

            }
            if ("created".equalsIgnoreCase(status)) {
                model.addAttribute("created", "Account created successfully!");
                registerDTO = new RegisterDTO();
                session.setAttribute("account", registerDTO);
                model.addAttribute("account", registerDTO);
            }
            if ("duplicated".equalsIgnoreCase(status)) {
                errorUsername = "Duplicate username!";
                model.addAttribute("errorUsername", errorUsername);
                model.addAttribute("account", registerDTO);
                model.addAttribute("msg", "Error!");
            }


            sort = pageable.getSort();
            if (pageable.getPageNumber() > 0) {
                pageNumber = pageable.getPageNumber() - 1;
            }
            pageable = PageRequest.of(pageNumber, pageSize, sort);
            accountPage = accountService.accountGetByPage(pageable, new QueryDTO());

            model.addAttribute("accountList", accountPage);
            model.addAttribute("pageData", accountPage);

            model.addAttribute("gendersList", GENDER_LIST);
            model.addAttribute("typesList", TYPE_LIST);

            model.addAttribute("typeGet", "get");
            model.addAttribute("queryDTO", new QueryDTO());

            totalPages = accountPage.getTotalPages();
            model.addAttribute("totalPages", totalPages);

//            return "redirect:/mvc/admin/account-management/paging";
            return "master/account-management";

        } catch (Exception e) {
//            String errorEmail = "Duplicate email!";


            sort = pageable.getSort();
            if (pageable.getPageNumber() > 0) {
                pageNumber = pageable.getPageNumber() - 1;
            }
            pageable = PageRequest.of(0, pageSize, sort);
            accountPage = accountService.accountGetByPage(pageable, new QueryDTO());

            model.addAttribute("account", registerDTO);
            model.addAttribute("accountList", accountPage);
            model.addAttribute("pageData", accountPage);

            model.addAttribute("gendersList", GENDER_LIST);
            model.addAttribute("typesList", TYPE_LIST);

            model.addAttribute("typeGet", "get");
            model.addAttribute("queryDTO", new QueryDTO());

            totalPages = accountPage.getTotalPages();
            model.addAttribute("totalPages", totalPages);
//            model.addAttribute("errorEmail", errorEmail);
            model.addAttribute("error", "Error!");
//            model.addAttribute("error", e.toString());
//            return "redirect:/mvc/admin/account-management/paging";
            return "master/account-management";
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management/details/{id}")
    public String accountDetails(@PathVariable("id") Long id, Model model) {

        Account account = accountService.getById(id);
        model.addAttribute("account", account);

        model.addAttribute("gendersList", GENDER_LIST);
        model.addAttribute("typesList", TYPE_LIST);

        return "master/account-details";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management/export-excel")
    public void downloadExcel(HttpServletResponse response, @PathParam("search") QueryDTO queryDTO, @RequestParam(defaultValue = "updatedDate") String fieldSort, @RequestParam(defaultValue = "desc") String direction) throws IOException {
        accountService.exportExcelAccounts(response, queryDTO, fieldSort, direction);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/account-management/export-pdf")
    public void downloadPDF(HttpServletResponse response, @PathParam("search") QueryDTO queryDTO, @RequestParam(defaultValue = "updatedDate") String fieldSort, @RequestParam(defaultValue = "desc") String direction) throws IOException {
        accountService.exportPDFAccounts(response, queryDTO, fieldSort, direction);
    }

}
