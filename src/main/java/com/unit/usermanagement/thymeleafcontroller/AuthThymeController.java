package com.unit.usermanagement.thymeleafcontroller;

import com.unit.usermanagement.dto.LoginDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.exception.StringValidator;
import com.unit.usermanagement.service.IAccountService;
import com.unit.usermanagement.service.IAuthService;
import com.unit.usermanagement.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Controller
@RequestMapping("/mvc/auth")
public class AuthThymeController {

    @Autowired
    private IAuthService authService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtilities;

    @GetMapping("/login")
    public String loginPage(Model model) {
        return "/auth/login";
    }

    @PostMapping("/login")
    public String processLoginForm(@ModelAttribute("loginForm") LoginDTO loginForm, RedirectAttributes redirectAttributes, Model model, HttpSession session) {
        String username = loginForm.getUsername();
        String password = loginForm.getPassword();
        String message = ""; // Giá trị mặc định cho biến message

        try {
            if (!StringValidator.isSpecialCharacters(username)) {
                message += "The username must not contain special characters!\n";
            }
            if (password.length() < 6) {
                message += "\nPassword must be 6 or more characters!\n";
            }

            if (message != "") {
                model.addAttribute("message", message);
                return "/auth/login";
            }

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginForm.getUsername(), loginForm.getPassword())
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String usernameLogined = authentication.getName();

            Account accountLogined = accountService.findByUsername(usernameLogined);

            boolean hasUserRole = authentication.getAuthorities()
                    .stream()
                    .anyMatch(authority -> "USER".equals(authority.getAuthority()));

            boolean hasAdminRole = authentication.getAuthorities()
                    .stream()
                    .anyMatch(authority -> "ADMIN".equals(authority.getAuthority()));


            if (accountLogined != null && hasUserRole && !hasAdminRole) {
                redirectAttributes.addFlashAttribute("accountLogined", accountLogined);
                session.setAttribute("accountLogined", accountLogined);
                return "redirect:/mvc/home/index";
//                return "/home";
            } else if (accountLogined != null && hasAdminRole) {

                redirectAttributes.addFlashAttribute("accountLogined", accountLogined);
                session.setAttribute("accountLogined", accountLogined);
                return "redirect:/mvc/admin/account-management";

//                return "/master/account-management";
            } else {
                SecurityContextHolder.clearContext();

                message += "Incorrect username or password\n";

                model.addAttribute("message", message);
                return "/auth/login";
            }
        } catch (Exception e) {
            SecurityContextHolder.clearContext();

            message += "Incorrect username or password\n";

            model.addAttribute("message", message);
            message = "";
            return "/auth/login";

        }
    }

    @GetMapping("/logout")
    public String logout() {
        authService.logout();
        return "redirect:/mvc/auth/login";
    }

    @GetMapping("/google/success")
    public CompletableFuture<String> googleCallback(@RequestParam(value = "code", defaultValue = "null") String code, Model model) {
//        Map<String, String> results = new HashMap<>();
        try {
            CompletableFuture<Map<String, String>> asyncResults = authService.googleCallBack(code, "MVC");

            return asyncResults.thenApply(results -> {
                if ("success".equals(results.get("status"))) {
                    model.addAttribute("emailLogined", results.get("email"));
                    model.addAttribute("accessToken", results.get("accessToken"));
                    model.addAttribute("refreshToken", results.get("refreshToken"));
                    return "/auth/login-success";
                } else {
                    return "/auth/login";
                }
            });
        } catch (UsernameNotFoundException e) {
            // Xử lý trường hợp không tìm thấy tên người dùng (tùy chọn)

            model.addAttribute("status", "failed");
            model.addAttribute("description", "Your email is not linked to the system!");
//            model.addAttribute("emailLogined", "");
//            model.addAttribute("accessToken", "");
//            model.addAttribute("refreshToken", "");
            CompletableFuture completableFuture = new CompletableFuture<>();
            return completableFuture.thenApply(restTemplate -> {
                return "/auth/login";
            });

        } catch (Exception e) {
            // Xử lý các ngoại lệ khác có thể xảy ra trong quá trình xử lý
            CompletableFuture completableFuture = new CompletableFuture<>();
            return completableFuture.thenApply(restTemplate -> {
                return "/auth/login";
            });
        }
    }


}
