package com.unit.usermanagement.thymeleafcontroller;

import com.unit.usermanagement.dto.FileDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.service.IAccountService;
import com.unit.usermanagement.utils.FileUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "/mvc/file/admin")
public class FileThymeController {

    @Autowired
    private IAccountService accountService;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/upload-accounts-excel")
    public String uploadPage() {
        return "redirect:/mvc/admin/account-management";
    }

    @PostMapping("/upload-accounts-excel")
    public String readAccountsFromExcel(@ModelAttribute FileDTO fileUpload, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        MultipartFile file = fileUpload.getFile();

        try {
            Account accountLogined = (Account) session.getAttribute("accountLogined");

//            long lastId = accountService.findMaxId();
//            List<Account> accountList = FileUtils.readAccountsFromExcel(file.getInputStream(), lastId, accountLogined); // Sử dụng lớp ExcelReader để đọc dữ liệu từ Excel
            List<Account> accountList = FileUtils.readAccountsFromExcel(file.getInputStream(), accountLogined); // Sử dụng lớp ExcelReader để đọc dữ liệu từ Excel
            boolean status = accountService.saveAccountsFromExcel(accountList);

            redirectAttributes.addFlashAttribute("status", status);

            return "redirect:/mvc/admin/account-management/paging"; // Trả về template để hiển thị dữ liệu

        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());

            return "redirect:/mvc/admin/account-management/paging"; // Trả về template báo lỗi
        }
    }
}
