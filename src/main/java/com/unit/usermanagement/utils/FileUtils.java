package com.unit.usermanagement.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.unit.usermanagement.entity.Account;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FileUtils {

    private static final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    public static String exportFileName(String defaultName, String fileType) {

        Date createdDate = new Date();
        LocalDate localDate = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalTime localTime = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();

        String fileName = "";

        String month = (localDate.getMonthValue() < 10) ? "0" + localDate.getMonthValue() : String.valueOf(localDate.getMonthValue());
        String date = (localDate.getDayOfMonth() < 10) ? "0" + localDate.getDayOfMonth() : String.valueOf(localDate.getDayOfMonth());
        String hour = (localTime.getHour() < 10) ? "0" + localTime.getHour() : String.valueOf(localTime.getHour());
        String minute = (localTime.getMinute() < 10) ? "0" + localTime.getMinute() : String.valueOf(localTime.getMinute());
        String second = (localTime.getSecond() < 10) ? "0" + localTime.getSecond() : String.valueOf(localTime.getSecond());
        fileName = defaultName + localDate.getYear() + month + date + "_" + hour + minute + second + fileType;

        return fileName;
    }

    public static PdfPCell createHeaderCellPDF(String text, Font font) {
        PdfPCell headerCell = new PdfPCell(new Phrase(text, font));
        headerCell.setBackgroundColor(BaseColor.GRAY);
        headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        headerCell.setPadding(8);
        return headerCell;
    }

    public static CellStyle createHeaderCellExcel(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        org.apache.poi.ss.usermodel.Font font = workbook.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.WHITE.getIndex()); // Set font color to white
        font.setFontHeightInPoints((short) 14); // Set font size
        style.setFont(font);
        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex()); // Set background color
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.CENTER); // Center-align header text
        style.setVerticalAlignment(VerticalAlignment.CENTER); // Vertically align header text
        return style;
    }


    public static List<Account> readAccountsFromExcel(InputStream inputStream, Account accountLogined) throws IOException {
        List<Account> accountList = new ArrayList<>();
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);
//        Long Id = lastId;
        Iterator<Row> rowIterator = sheet.iterator();
        if (rowIterator.hasNext()) {
            rowIterator.next(); // Bỏ qua hàng đầu tiên
        }

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Account account = new Account();

            Cell cell1 = row.getCell(1);
            Cell cell2 = row.getCell(2);
            Cell cell3 = row.getCell(3);
            Cell cell4 = row.getCell(4);
            Cell cell5 = row.getCell(5);
            Cell cell6 = row.getCell(6);
            Cell cell7 = row.getCell(7);

            if (accountLogined != null) {
                account.setCreatedId(accountLogined.getId());

            }
            account.setDeletedFlag(false);
            account.setActived(true);
            account.setCreatedDate(new Date());
            account.setUsername(cell1 != null ? cell1.getStringCellValue() : "");
            account.setPassword(cell2 != null ? bCryptPasswordEncoder.encode(cell2.getStringCellValue()) : "");
            account.setEmail(cell3 != null ? cell3.getStringCellValue() : "");
            account.setPhone(cell4 != null ? cell4.getStringCellValue() : "");
            account.setFullName(cell5 != null ? cell5.getStringCellValue() : "");
            account.setBirthday(cell6 != null ? cell6.getDateCellValue() : null);
            account.setGender(cell7 != null ? cell7.getStringCellValue() : "");

            accountList.add(account);
        }

        workbook.close();
        return accountList;
    }

    public static List<Account> readAccountsFromExcel(InputStream inputStream, long lastId, Account accountLogined) throws IOException {
        List<Account> accountList = new ArrayList<>();
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);
//        Long Id = lastId;
        Iterator<Row> rowIterator = sheet.iterator();
        if (rowIterator.hasNext()) {
            rowIterator.next(); // Bỏ qua hàng đầu tiên
        }

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Account account = new Account();

            Cell cell1 = row.getCell(1);
            Cell cell2 = row.getCell(2);
            Cell cell3 = row.getCell(3);
            Cell cell4 = row.getCell(4);
            Cell cell5 = row.getCell(5);
            Cell cell6 = row.getCell(6);
            Cell cell7 = row.getCell(7);

            account.setId(lastId++);
            if (accountLogined != null) {
                account.setCreatedId(accountLogined.getId());

            }
            account.setDeletedFlag(false);
            account.setActived(true);
            account.setCreatedDate(new Date());
            account.setUsername(cell1 != null ? cell1.getStringCellValue() : "");
            account.setPassword(cell2 != null ? bCryptPasswordEncoder.encode(cell2.getStringCellValue()) : "");
            account.setEmail(cell3 != null ? cell3.getStringCellValue() : "");
            account.setPhone(cell4 != null ? cell4.getStringCellValue() : "");
            account.setFullName(cell5 != null ? cell5.getStringCellValue() : "");
            account.setBirthday(cell6 != null ? cell6.getDateCellValue() : null);
            account.setGender(cell7 != null ? cell7.getStringCellValue() : "");

            accountList.add(account);
        }

        workbook.close();
        return accountList;
    }
}
