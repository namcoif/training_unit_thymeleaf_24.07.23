package com.unit.usermanagement.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class IPUtils {
    public static String getCountryFromIP(String ipAddress) throws Exception {
        String url = "http://ipinfo.io/" + ipAddress + "/country";

        HttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);

        HttpResponse response = client.execute(request);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String country = rd.readLine();

        return country;
    }
}
