package com.unit.usermanagement.utils;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class TextUtils {

    private static Translate translate;


    public TextUtils(String apiKey) {
        translate = TranslateOptions.newBuilder().setApiKey(apiKey).build().getService();
    }

    public static String translateLanguage(String text, String targetLanguage) {
        Translation translation = translate.translate(text, Translate.TranslateOption.sourceLanguage(targetLanguage));
        return translation.getTranslatedText();
    }
}
