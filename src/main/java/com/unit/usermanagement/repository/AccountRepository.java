package com.unit.usermanagement.repository;

import com.unit.usermanagement.dto.AccountDTO;
import com.unit.usermanagement.dto.QueryDTO;
import com.unit.usermanagement.entity.Account;
import jp.xet.springframework.data.mirage.repository.MirageRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;


public interface AccountRepository extends MirageRepository<Account, Long> {
    Account findByUsernameAndPassword(
            @Param("username") String username,
            @Param("password") String password);

    Account findByUsername(@Param("username") String username);

    Account findByEmail(@Param("email") String email);

    Page<AccountDTO> getByPage(
            @Param("queryDTO") QueryDTO queryDTO,
//            @Param("username") String username,
//            @Param("fullName") String fullName,
//            @Param("birthday") Date birthday,
//            @Param("accountType") String accountType,
//            @Param("actived") String actived,
            Pageable pageable
    );

    long findMaxId();
}
