package com.unit.usermanagement.repository;

import com.unit.usermanagement.dto.RoleDTO;
import com.unit.usermanagement.entity.Role;
import jp.xet.springframework.data.mirage.repository.MirageRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoleRepository extends MirageRepository<Role, Integer> {
    List<RoleDTO> getByUsername(@Param("username") String username);

    void createRole(@Param("user_id") Long userId);
}
