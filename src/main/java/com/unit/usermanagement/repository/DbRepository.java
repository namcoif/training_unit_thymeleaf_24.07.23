package com.unit.usermanagement.repository;

import jp.xet.sparwings.spring.data.repository.BatchReadableRepository;
import jp.xet.sparwings.spring.data.repository.BatchWritableRepository;
import jp.xet.sparwings.spring.data.repository.ScannableRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface DbRepository<E, ID extends Serializable> extends ScannableRepository<E, ID>, BatchReadableRepository<E, ID>, BatchWritableRepository<E, ID> {
}
