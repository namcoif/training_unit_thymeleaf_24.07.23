package com.unit.usermanagement.repository;

import com.unit.usermanagement.entity.User;
import jp.xet.springframework.data.mirage.repository.MirageRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends MirageRepository<User, Long> {

    Page<User> findByUsernameMaxLength(
            @Param("username_length") int len,
            @Param("size") int size,
            Pageable pageable);

    Page<User> getByPage(
            @Param("username") String username,
            Pageable pageable);
}
