package com.unit.usermanagement.controller;

import com.unit.usermanagement.dto.AccountDTO;
import com.unit.usermanagement.dto.ErrorDTO;
import com.unit.usermanagement.dto.QueryDTO;
import com.unit.usermanagement.dto.RegisterDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.exception.StringValidator;
import com.unit.usermanagement.service.IAccountService;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/accounts")
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/get-all")
    public ResponseEntity<?> accountGetAll() {
        Iterable<Account> accountList = accountService.accountGetAll();
        return ResponseEntity.ok(accountList);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/paging")
    public ResponseEntity<?> accountGetByPage(Pageable pageable, @PathParam(value = "search") QueryDTO queryDTO) {
        try {
            Page<AccountDTO> accountPage = accountService.accountGetByPage(pageable, queryDTO);
            List<AccountDTO> listAccountDTOs = modelMapper.map(accountPage.getContent(), new TypeToken<List<AccountDTO>>() {
            }.getType());

//            Page<AccountDTO> accountsDTOPage = new PageImpl<>(listAccountDTOs, pageable, accountPage.getTotalElements());
            long totalElements = 0;
            Page<AccountDTO> accountsDTOPage = new PageImpl<>(listAccountDTOs, pageable, totalElements);
            if (accountPage.getContent().size() > 0) {
                totalElements = accountPage.getContent().get(0).getTotalElements().longValue();
                accountsDTOPage = new PageImpl<>(listAccountDTOs, pageable, totalElements);
            }
            return ResponseEntity.status(HttpStatus.OK).body(accountsDTOPage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.toString());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/management/paging")
    public ResponseEntity<?> accountSearchByPage(Pageable pageable, @PathParam(value = "search") QueryDTO queryDTO) {

        try {
            Page<AccountDTO> accountPage = accountService.accountGetByPage(pageable, queryDTO);
            List<AccountDTO> listAccountDTOs = modelMapper.map(accountPage.getContent(), new TypeToken<List<AccountDTO>>() {
            }.getType());

            Page<AccountDTO> accountsDTOPage = new PageImpl<>(listAccountDTOs, pageable, accountPage.getTotalElements());

            return ResponseEntity.status(HttpStatus.OK).body(accountsDTOPage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.toString());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/{Id}")
//    @ApiOperation("Get account by ID")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "Success", response = Account.class),
//            @ApiResponse(code = 404, message = "User not found")
//    })
    public ResponseEntity<?> accountGetById(@PathVariable("Id") Long Id) {

        try {

            Account account = accountService.getById(Id);
            return ResponseEntity.status(HttpStatus.OK).body(account);

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error");
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/management/delete/{Id}")
    public ResponseEntity<?> accountDeleteById(@PathVariable("Id") Long Id, @PathParam("deleteId") Long deleteId) {
        JSONObject msg = new JSONObject();
        try {
            accountService.deleteById(Id, deleteId);
            msg.put("msg", "Account has been deleted");

            return ResponseEntity.status(HttpStatus.OK).body(msg.toString());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/management/save")
    public ResponseEntity<?> saveAccount(@RequestBody RegisterDTO registerDTO) {
        boolean isInvalid = false;
        List<String> errorsUsername = new ArrayList<>();
        List<String> errorsPassword = new ArrayList<String>();
        ErrorDTO errors = new ErrorDTO();
        if (!StringValidator.isSpecialCharacters(registerDTO.getUsername())) {
            isInvalid = true;
            errorsUsername.add("The username must not contain special characters!");
        }
        if ("".equals(registerDTO.getUsername()) || registerDTO.getUsername() == null) {
            isInvalid = true;
            errorsUsername.add("The username must not is blank!");
        }
        if (registerDTO.getPassword() != null && registerDTO.getPassword().length() < 6) {
            isInvalid = true;
            errorsPassword.add("Password must be 6 or more characters!");
        }
        if (registerDTO.getPassword() == null) {
            isInvalid = true;
            errorsPassword.add("Password must be 6 or more characters!");
        }

        if (isInvalid) {
            errors.setUsername(errorsUsername);
            errors.setPassword(errorsPassword);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
        }

        try {
            Account account = modelMapper.map(registerDTO, Account.class);
            Account accAvailbe = accountService.findByUsername(registerDTO.getUsername());
            
            if (account.getId() == null && accAvailbe == null) {
                Account accountCreated = accountService.createAccount(account);
                return ResponseEntity.status(HttpStatus.CREATED).body(accountCreated);

            } else if (account.getId() == null && account.getUsername().equals(accAvailbe.getUsername())) {
                errorsUsername.add("Duplicate username!");
            } else if (account.getId() != null && accAvailbe == null) {
                Account accountUpdated = accountService.updateAccount(account);
                return ResponseEntity.status(HttpStatus.OK).body(accountUpdated);

            } else if (account.getUsername().equals(accAvailbe.getUsername()) && account.getId().equals(accAvailbe.getId())) {
                Account accountUpdated = accountService.updateAccount(account);
                return ResponseEntity.status(HttpStatus.OK).body(accountUpdated);
            } else {
                errorsUsername.add("Duplicate username!");
            }

            errors.setUsername(errorsUsername);
            errors.setPassword(errorsPassword);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/details/{Id}")
    public ResponseEntity<?> accountDetails(@PathVariable("Id") Long Id) {
        try {
            Account account = accountService.getById(Id);

            return ResponseEntity.status(HttpStatus.OK).body(account);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/export-excel")
    public void exportExcel(HttpServletResponse response, @PathParam(value = "search") QueryDTO queryDTO, @RequestParam(defaultValue = "updatedDate") String fieldSort, @RequestParam(defaultValue = "desc") String direction) throws IOException {

        accountService.exportExcelAccounts(response, queryDTO, fieldSort, direction);

    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/export-pdf")
    public void exportPDF(HttpServletResponse response, @PathParam(value = "search") QueryDTO queryDTO, @RequestParam(defaultValue = "updatedDate") String fieldSort, @RequestParam(defaultValue = "desc") String direction) throws IOException {

        accountService.exportPDFAccounts(response, queryDTO, fieldSort, direction);

    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/management/export-doc")
    public void exportDoc(HttpServletResponse response, @PathParam(value = "search") QueryDTO queryDTO, @RequestParam(defaultValue = "updatedDate") String fieldSort, @RequestParam(defaultValue = "desc") String direction) throws IOException {

        accountService.exportDocAccounts(response, queryDTO, fieldSort, direction);

    }
}
