package com.unit.usermanagement.controller;

import com.unit.usermanagement.configuration.CustomUserDetailsService;
import com.unit.usermanagement.dto.AccountDTO;
import com.unit.usermanagement.dto.JwtTokenDTO;
import com.unit.usermanagement.dto.LoginDTO;
import com.unit.usermanagement.dto.RegisterDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.service.IAccountService;
import com.unit.usermanagement.service.IAuthService;
import com.unit.usermanagement.service.IRoleService;
import com.unit.usermanagement.utils.JwtUtils;
import org.json.JSONException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "api/v1/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IAuthService authService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JwtUtils jwtUtilities;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String GOOGLE_CLIENT_ID;

    @Value("${spring.security.oauth2.client.registration.google.client-secret}")
    private String GOOGLE_CLIENT_SECRET;

    //    @Value("${spring.security.oauth2.client.registration.google.redirect-uri}")
    private static final String REDIRECT_URI = "http://localhost:8080/api/v1/auth/google/success";

    @PostMapping("/register")
    public ResponseEntity<?> registerAccount(@RequestBody RegisterDTO registerDTO) {
        try {
            Account account = modelMapper.map(registerDTO, Account.class);
//            account.setAccountType("USER");
            Account accountCreated = accountService.createAccount(account);
            roleService.createRole(accountCreated.getId());
            AccountDTO accountDTO = modelMapper.map(accountCreated, AccountDTO.class);

            return ResponseEntity.status(HttpStatus.CREATED).body(accountDTO);

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    //    @PostMapping("/login")
//    public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO) {
//        try {
//            Account accountLogined = accountService.login(loginDTO);
//            AccountDTO accountDTO = modelMapper.map(accountLogined, AccountDTO.class);
//            return ResponseEntity.status(HttpStatus.OK).body(accountDTO);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//        }
//    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginDTO loginDTO) throws JSONException {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword())
            );

//            SecurityContextHolder.getContext().setAuthentication(authentication);

            String accessToken = jwtUtilities.generateAccessToken(authentication);
            String refreshToken = jwtUtilities.generateRefreshToken(authentication);

            UserDetails userDetails = (UserDetails) authentication.getPrincipal();

            Account account = accountService.findByUsername(userDetails.getUsername());

            return ResponseEntity.ok(
                    new JwtTokenDTO(userDetails.getUsername(), accessToken, refreshToken, userDetails.getAuthorities().toString(), account.getId().toString())
            );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.toString());
        }
    }

    @GetMapping("/logout")
    public ResponseEntity<?> logout() throws JSONException {
        try {
            authService.logout();
            return ResponseEntity.ok("Logout");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.toString());
        }
    }

    @GetMapping("/google")
    public ResponseEntity<String> googleAuth() {
        // Chuyển hướng người dùng đến trang xác thực của Google
        String redirectUrl = authService.googleAuth("API");
        System.out.println(redirectUrl);
        return ResponseEntity.status(HttpStatus.FOUND)
                .header("Location", redirectUrl)
                .build();
    }

    @GetMapping("/google/success")
    public CompletableFuture<ResponseEntity<?>> googleCallback(@RequestParam("code") String code) {
        CompletableFuture<Map<String, String>> asyncResults = new CompletableFuture<>();

        try {
            asyncResults = authService.googleCallBack(code, "API");

            return asyncResults.thenApply(results -> {
                if (
                        "success".equals(results.get("status"))) {
                    return ResponseEntity.status(HttpStatus.OK).body(results);
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(results);
                }
            });

        } catch (UsernameNotFoundException e) {
            // Xử lý trường hợp không tìm thấy tên người dùng (tùy chọn)
            return asyncResults.thenApply(results -> {
                results.put("status", "failed");
                results.put("description", "Your email is not linked to the system!");
                results.put("email", "");
                results.put("accessToken", "");
                results.put("refreshToken", "");

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(results);
            });

        } catch (Exception e) {
            // Xử lý các ngoại lệ khác có thể xảy ra trong quá trình xử lý
            return asyncResults.thenApply(results -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error!"));
        }
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<Map<String, String>> refreshToken(@RequestParam("refreshToken") String refreshToken) {
        try {
            String username = jwtUtilities.getUsernameByToken(refreshToken);

            // Kiểm tra tính hợp lệ của refresh token
            if (jwtUtilities.validateJwtToken(refreshToken)) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );
                // Tạo access token mới
                String newAccessToken = jwtUtilities.generateAccessToken(authentication);

                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", newAccessToken);

                return ResponseEntity.ok(tokens);
            } else {
                throw new AuthenticationException("Invalid refresh token") {
                };
            }
        } catch (Exception e) {
            throw new AuthenticationException("Invalid refresh token") {
            };
        }
    }
}
