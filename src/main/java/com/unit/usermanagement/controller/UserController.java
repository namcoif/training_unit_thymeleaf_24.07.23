package com.unit.usermanagement.controller;

import com.unit.usermanagement.entity.User;
import com.unit.usermanagement.service.IUserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/v1/users")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/management/get-all")
    public ResponseEntity<?> userGetAll() {
        Iterable<User> users = userService.userGetAll();
        return ResponseEntity.ok(users);
    }

    @GetMapping("/management/get-maxusername")
    public ResponseEntity<?> userGetByMaxUserName(Pageable pageable) {
        Iterable<User> users = userService.findByUsernameMaxLength(8, 5, pageable);
        return ResponseEntity.ok(users);
    }
}
