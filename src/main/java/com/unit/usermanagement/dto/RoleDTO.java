package com.unit.usermanagement.dto;

import com.miragesql.miragesql.annotation.Column;
import lombok.Data;

@Data
public class RoleDTO {

    @Column(name = "id")
    private Integer id;

    @Column(name = "role")
    private String role;

    @Column(name = "user_id")
    private Long userId;
}
