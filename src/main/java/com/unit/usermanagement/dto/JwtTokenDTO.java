package com.unit.usermanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtTokenDTO {
    private String username;
    private String accessToken;
    private String refreshToken;
    private String role;
    private String userId;
}
