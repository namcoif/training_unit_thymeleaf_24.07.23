package com.unit.usermanagement.dto;

import com.miragesql.miragesql.annotation.Column;
import com.unit.usermanagement.entity.Account;
import lombok.Data;

@Data
public class AccountDTO extends Account {

    @Column(name = "creater")
    private String creater;

    @Column(name = "updater")
    private String updater;

    @Column(name = "totalElements")
    private Integer totalElements;

    @Column(name = "totalPages")
    private Integer totalPages;
}
