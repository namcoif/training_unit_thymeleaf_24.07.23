package com.unit.usermanagement.dto;

public interface UserProductDTO {
    Integer getId();

    String getName();

    Integer getUserId();

    String getUsername();

//    private Integer id;
//    private String name;
//    private Integer userId;
//    private String username;


}
