package com.unit.usermanagement.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class QueryDTO {
    private String username;
    private String fullName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String accountType;
    private String actived;
//    private Integer pageNumber = 1;
//    private Integer size = 10;
}
