package com.unit.usermanagement.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FileDTO {
    private MultipartFile file;

}
