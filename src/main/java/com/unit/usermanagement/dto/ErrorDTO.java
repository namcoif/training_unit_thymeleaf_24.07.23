package com.unit.usermanagement.dto;

import lombok.Data;

import java.util.List;

@Data
public class ErrorDTO {
    private List<String> username;
    private List<String> password;
    private List<String> orther;
}
