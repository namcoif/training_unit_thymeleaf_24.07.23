//package com.unit.usermanagement.specification;
//
//import com.unit.usermanagement.entity.Account;
//import jakarta.persistence.criteria.CriteriaBuilder;
//import jakarta.persistence.criteria.CriteriaQuery;
//import jakarta.persistence.criteria.Predicate;
//import jakarta.persistence.criteria.Root;
//import lombok.Data;
//import org.springframework.data.jpa.domain.Specification;
//
//
//@Data
//public class CustomAccountSpecification implements Specification<Account> {
//
//    private String field;
//
//    private Object value;
//
//    public CustomAccountSpecification(String field, Object value) {
//        this.field = field;
//        this.value = value;
//    }
//
//
//    @Override
//    public Predicate toPredicate(Root<Account> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
//        if (field.equalsIgnoreCase("deletedFlag")) {
//            return criteriaBuilder.isFalse(root.get("deletedFlag"));
//        }
//        if (field.equalsIgnoreCase("username")) {
//            return criteriaBuilder.like(root.get("username"), "%" + value + "%");
//        }
//        if (field.equalsIgnoreCase("fullName")) {
//            return criteriaBuilder.like(root.get("fullName"), "%" + value + "%");
//        }
//        if (field.equalsIgnoreCase("birthday")) {
//            return criteriaBuilder.equal(root.get("birthday"), value);
//        }
//        if (field.equalsIgnoreCase("accountType")) {
//            if ("all".equals(value)) {
//                return criteriaBuilder.conjunction();
//            } else {
//                return criteriaBuilder.like(root.get("accountType"), "%" + value + "%");
//            }
//        }
//        if (field.equalsIgnoreCase("actived")) {
//            if ("all".equals(value)) {
//                return criteriaBuilder.or(criteriaBuilder.isFalse(root.get("actived")), criteriaBuilder.isTrue(root.get("actived")));
//            }
//            if ("true".equals(value)) {
//                return criteriaBuilder.isTrue(root.get("actived"));
//            }
//            if ("false".equals(value)) {
//                return criteriaBuilder.isFalse(root.get("actived"));
//            }
//        }
//        return null;
//    }
//}
