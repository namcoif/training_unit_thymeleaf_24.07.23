//package com.unit.usermanagement.specification;
//
//import com.unit.usermanagement.dto.QueryDTO;
//import com.unit.usermanagement.entity.Account;
//import jakarta.persistence.criteria.CriteriaBuilder;
//import lombok.Data;
//import org.springframework.data.jpa.domain.Specification;
//import org.springframework.util.StringUtils;
//
//@Data
//public class AccountSpecification {
//    public static Specification<Account> searchAccount(QueryDTO queryDTO) {
//        Specification<Account> where = null;
//        CriteriaBuilder criteriaBuilder = null;
//
//        CustomAccountSpecification deletedFlag = new CustomAccountSpecification("deletedFlag", false);
//        where = Specification.where(deletedFlag);
//
//        if (queryDTO.getUsername() != null && !StringUtils.isEmpty(queryDTO.getUsername())) {
//            CustomAccountSpecification username = new CustomAccountSpecification("username", queryDTO.getUsername().trim());
//            if (where != null) {
//                where = where.and(username);
//            } else {
//                where = Specification.where(username);
//            }
//        }
//        if (queryDTO.getFullName() != null && !StringUtils.isEmpty(queryDTO.getFullName())) {
//            CustomAccountSpecification fullName = new CustomAccountSpecification("fullName", queryDTO.getFullName().trim());
//            if (where != null) {
//                where = where.and(fullName);
//            } else {
//                where = Specification.where(fullName);
//            }
//        }
//        if (queryDTO.getBirthday() != null) {
//            CustomAccountSpecification birthday = new CustomAccountSpecification("birthday", queryDTO.getBirthday());
//            if (where != null) {
//                where = where.and(birthday);
//            } else {
//                where = Specification.where(birthday);
//            }
//        }
//        if (queryDTO.getAccountType() != null && !StringUtils.isEmpty(queryDTO.getAccountType())) {
//            CustomAccountSpecification accountType = new CustomAccountSpecification("accountType", queryDTO.getAccountType().trim());
//            if (where != null) {
//                where = where.and(accountType);
//            } else {
//                where = Specification.where(accountType);
//            }
//        }
//        if (queryDTO.getActived() != null && !StringUtils.isEmpty(queryDTO.getActived())) {
//            CustomAccountSpecification actived = new CustomAccountSpecification("actived", queryDTO.getActived());
//            if (where != null) {
//                where = where.and(actived);
//            } else {
//                where = Specification.where(actived);
//            }
//        }
//
//        return where;
//    }
//}
