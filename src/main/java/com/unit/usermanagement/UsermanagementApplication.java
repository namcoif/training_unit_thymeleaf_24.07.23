package com.unit.usermanagement;

//import com.unit.usermanagement.entity.User;
//import com.unit.usermanagement.repository.IUserRepository;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsermanagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsermanagementApplication.class, args);
    }

//    @SuppressWarnings("deprecation")
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            public void addCorsMappings(CorsRegistry registry) {
//                registry
//                        .addMapping("/**")
//                        .allowedOrigins("*")
//                        .allowedMethods("GET", "POST", "PUT", "DELETE");
//            }
//        };
//    }
}

//@Component
//@ComponentScan
//public class UsermanagementApplication {
//    @Autowired
//    JdbcTemplate jdbcTemplate;
//    private static final Logger logger = LoggerFactory.getLogger(UsermanagementApplication.class);
//
//    public static void main(String[] args) {
//        try (ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(UsermanagementApplication.class)) {
//            UsermanagementApplication das = context.getBean(UsermanagementApplication.class);
//            das.execute();
//        }
//    }
//
//    @Autowired
//    IUserRepository userRepository;
//
//
//    @Transactional(transactionManager = "transactionManager")
//    public void execute() {
////        long idl = 12L;
////        Long id = Long.valueOf(idl);
////        // create
////        userRepository.save(new User(id, "nam", "$2a$10$MHPqWJ61alnBlUbvjEGK/uWRvwtYzolWCuFXW8YMJkT54HUB0H9iq"));
//
//        // read
//        logger.info("List user");
//        Iterable<User> all = userRepository.findAll();
//        for (User user : all) {
//            logger.info("  {}", user);
//        }
//    }
//
//}
