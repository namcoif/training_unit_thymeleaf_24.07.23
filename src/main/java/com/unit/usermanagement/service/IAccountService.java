package com.unit.usermanagement.service;

import com.unit.usermanagement.dto.AccountDTO;
import com.unit.usermanagement.dto.QueryDTO;
import com.unit.usermanagement.dto.RegisterDTO;
import com.unit.usermanagement.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface IAccountService {
    Iterable<Account> accountGetAll();

    Account createAccount(Account account);

    Account updateAccount(Account account);

    String saveAccount(Account accountSave, RegisterDTO accountModifying, Account accountLogined);

    Account getById(Long id);

    boolean deleteById(Long id, Long deletedId);

    Account findByUsername(String username);

    Page<AccountDTO> accountGetByPage(Pageable pageable, QueryDTO queryDTO);

    boolean isUsernameDuplicate(String username);

    void exportExcelAccounts(HttpServletResponse response, QueryDTO queryDTO, String fieldSort, String direction);

    void exportPDFAccounts(HttpServletResponse response, QueryDTO queryDTO, String fieldSort, String direction);

    void exportDocAccounts(HttpServletResponse response, QueryDTO queryDTO, String fieldSort, String direction);

    boolean saveAccountsFromExcel(List<Account> accountList);

    long findMaxId();
}
