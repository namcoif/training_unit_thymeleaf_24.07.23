package com.unit.usermanagement.service.impl;

import com.unit.usermanagement.configuration.CustomUserDetailsService;
import com.unit.usermanagement.dto.LoginDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.repository.AccountRepository;
import com.unit.usermanagement.service.IAuthService;
import com.unit.usermanagement.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional(transactionManager = "transactionManager")
public class AuthService implements IAuthService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String GOOGLE_CLIENT_ID;

    @Value("${spring.security.oauth2.client.registration.google.client-secret}")
    private String GOOGLE_CLIENT_SECRET;

    @Value("${spring.security.oauth2.client.registration.google.redirect-uri}")
    private String REDIRECT_URI_MVC;

    private final static String REDIRECT_URI_API = "http://localhost:8080/api/v1/auth/google/success";

    @Autowired
    private JwtUtils jwtUtilities;

    @Override
    public Account login(LoginDTO loginDTO) {
        try {
            Account account = null;
            account = accountRepository.findByUsernameAndPassword(loginDTO.getUsername(), loginDTO.getPassword());
            return account;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public CompletableFuture<Map<String, String>> googleCallBack(String code, String typeWeb) {
        Map<String, String> results = new HashMap<>();

        String tokenEndpoint = "https://oauth2.googleapis.com/token";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("code", code);
        body.add("client_id", GOOGLE_CLIENT_ID);
        body.add("client_secret", GOOGLE_CLIENT_SECRET);
        body.add("grant_type", "authorization_code");
        if ("MVC".equals(typeWeb)) {
            body.add("redirect_uri", REDIRECT_URI_MVC);
        } else if ("API".equals(typeWeb)) {
            body.add("redirect_uri", REDIRECT_URI_API);

        }

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Map> responseEntity = restTemplate.exchange(tokenEndpoint, HttpMethod.POST, request, Map.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            Map<String, Object> responseBody = responseEntity.getBody();
            String accessToken = (String) responseBody.get("access_token");

            String userInfoEndpoint = "https://www.googleapis.com/oauth2/v2/userinfo";
            HttpHeaders userInfoHeaders = new HttpHeaders();
            userInfoHeaders.setBearerAuth(accessToken);
            HttpEntity<Void> userInfoRequest = new HttpEntity<>(userInfoHeaders);

            ResponseEntity<Map> userInfoResponse = restTemplate.exchange(userInfoEndpoint, HttpMethod.GET, userInfoRequest, Map.class);

            if (userInfoResponse.getStatusCode() == HttpStatus.OK) {
                Map<String, Object> userInfo = userInfoResponse.getBody();
                String userEmail = (String) userInfo.get("email");

                // Check the userEmail against the data in your database and perform the login process
                UserDetails userDetails = userDetailsService.loadUserByEmail(userEmail);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                accessToken = jwtUtilities.generateAccessToken(authentication);
                String refreshToken = jwtUtilities.generateRefreshToken(authentication);

                results.put("status", "success");
                results.put("description", "Logged in with Google successfully!");
                results.put("email", userEmail);
                results.put("accessToken", accessToken);
                results.put("refreshToken", refreshToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } else {
                results.put("status", "failed");
                results.put("description", "Unable to retrieve user information from Google!");
                results.put("email", "");
                results.put("accessToken", "");
                results.put("refreshToken", "");
            }
        } else {
            results.put("status", "failed");
            results.put("description", "Google authentication failed!");
            results.put("email", "");
            results.put("accessToken", "");
            results.put("refreshToken", "");
        }
        return CompletableFuture.completedFuture(results);
    }

    @Override
    public void logout() {
        SecurityContextHolder.clearContext();
    }

    @Override
    public String googleAuth(String typeWeb) {
        String redirectUrl = "https://accounts.google.com/o/oauth2/v2/auth";
        if ("MVC".equals(typeWeb)) {
            redirectUrl = redirectUrl
                    + "?client_id=" + GOOGLE_CLIENT_ID
                    + "&redirect_uri=" + REDIRECT_URI_MVC
                    + "&response_type=code"
                    + "&scope=email%20profile";
        }

        if ("API".equals(typeWeb)) {
            redirectUrl = redirectUrl
                    + "?client_id=" + GOOGLE_CLIENT_ID
                    + "&redirect_uri=" + REDIRECT_URI_API
                    + "&response_type=code"
                    + "&scope=email%20profile";
        }

        return redirectUrl;
    }

}
