package com.unit.usermanagement.service.impl;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.unit.usermanagement.dto.AccountDTO;
import com.unit.usermanagement.dto.QueryDTO;
import com.unit.usermanagement.dto.RegisterDTO;
import com.unit.usermanagement.entity.Account;
import com.unit.usermanagement.repository.AccountRepository;
import com.unit.usermanagement.service.IAccountService;
import com.unit.usermanagement.utils.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
@Transactional(transactionManager = "transactionManager")
public class AccountService implements IAccountService {


    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ModelMapper modelMapper;
    public static BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    public static final Integer ROW_TABLE_FILE = 10000;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Override
    public Iterable<Account> accountGetAll() {

        return accountRepository.findAll();

    }

    @Override
    public Account createAccount(Account account) {

        String enCrypPassword = bCryptPasswordEncoder.encode(account.getPassword());
        account.setPassword(enCrypPassword);
        account.setDeletedFlag(false);
        account.setActived(true);
        account.setCreatedDate(new Date());
        accountRepository.create(account);

        return accountRepository.findByUsername(account.getUsername());
    }

    @Override
    public Account updateAccount(Account account) {
        Account accountUpdate = accountRepository.findOne(account.getId());
        String enCrypPassword = bCryptPasswordEncoder.encode(account.getPassword());
        if (!Objects.equals(account.getPassword(), accountUpdate.getPassword())) {
            accountUpdate.setPassword(enCrypPassword);
        }
        accountUpdate.setId(account.getId());
        accountUpdate.setUsername(account.getUsername());
        accountUpdate.setEmail(account.getEmail());
        accountUpdate.setFullName(account.getFullName());
        accountUpdate.setPhone(account.getPhone());
        accountUpdate.setBirthday(account.getBirthday());
        accountUpdate.setCreatedDate(accountUpdate.getCreatedDate());
        accountUpdate.setCreatedId(accountUpdate.getCreatedId());
        accountUpdate.setDeletedFlag(false);
        accountUpdate.setGender(account.getGender());
        accountUpdate.setAccountType(account.getAccountType());
        accountUpdate.setActived(account.isActived());
        if (account.getId() != null) {
            accountUpdate.setUpdatedId(account.getUpdatedId());
            accountUpdate.setUpdatedDate(new Date());
        }
        return accountRepository.update(accountUpdate);

    }

    @Override
    public String saveAccount(Account accountSave, RegisterDTO accountModifying, Account accountLogined) {
        if (accountModifying == null) {
            accountModifying = new RegisterDTO();
        }
        Account availbeAccount = findByUsername(accountModifying.getUsername());
        Account duplicateInputAccount = findByUsername(accountSave.getUsername());

        if (availbeAccount != null && accountSave.getUsername().equals(availbeAccount.getUsername()) && Objects.equals(accountModifying.getId(), availbeAccount.getId())) {
            accountSave.setId(accountModifying.getId());
            if (accountLogined != null) {
                accountSave.setUpdatedId(accountLogined.getId());
            }
            updateAccount(accountSave);
            return "updated";
        } else if (availbeAccount != null && duplicateInputAccount != null) {
            return "duplicated";
        } else if (availbeAccount != null && !accountSave.getUsername().equals(availbeAccount.getUsername()) && Objects.equals(accountModifying.getId(), availbeAccount.getId())) {
            accountSave.setId(accountModifying.getId());
            if (accountLogined != null) {
                accountSave.setUpdatedId(accountLogined.getId());
            }
            updateAccount(accountSave);
            return "updated";

        } else if (availbeAccount == null && duplicateInputAccount != null) {
            return "duplicated";

        } else {
            if (accountLogined != null) {
                accountSave.setCreatedId(accountLogined.getId());
            }
            createAccount(accountSave);
            return "created";
        }
    }

    @Override
    public Account getById(Long id) {
        return accountRepository.findOne(id);
    }

    @Override
    public boolean deleteById(Long id, Long deletedId) {
        if (!Objects.equals(id, deletedId) && deletedId != null) {
            Account accountDelete = accountRepository.findOne(id);
            Date deletedDate = new Date();
            accountDelete.setDeletedDate(deletedDate);
            accountDelete.setDeletedFlag(true);
            accountDelete.setDeletedId(deletedId);
            accountRepository.update(accountDelete);
            return true;
        }
        return false;

    }

    @Override
    public Account findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    public Page<AccountDTO> accountGetByPage(Pageable pageableRequest, QueryDTO queryDTORequest) {
        try {
            ExecutorService executorService = Executors.newFixedThreadPool(5);

            // Create a Callable to perform the asynchronous task
            Callable<Page<AccountDTO>> callableTask = () -> {
                // Your asynchronous code here
                Sort sortRequest = pageableRequest.getSort();

                if (sortRequest.isUnsorted()) {
                    sortRequest = Sort.by(
                            Sort.Order.desc("updatedDate"),
                            Sort.Order.desc("createdDate")
                    );
                }

                Pageable pageable = PageRequest.of(pageableRequest.getPageNumber(), pageableRequest.getPageSize(), sortRequest);

                return transactionTemplate.execute(status -> {
                    try {
                        List<AccountDTO> listAccountDTOs = modelMapper.map(accountRepository.getByPage(
                                queryDTORequest,
                                pageable
                        ).getContent(), new TypeToken<List<AccountDTO>>() {
                        }.getType());

                        long totalElements = listAccountDTOs.size() > 0 ? listAccountDTOs.get(0).getTotalElements().longValue() : 0;
                        return new PageImpl<>(listAccountDTOs, pageable, totalElements);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return new PageImpl<>(new ArrayList<AccountDTO>(), pageable, 0);
                    }
                });
            };

            // Submit the Callable to the executor
            Future<Page<AccountDTO>> futureResult = executorService.submit(callableTask);

            // Wait for the asynchronous task to complete and get the result
            Page<AccountDTO> result = futureResult.get();

            // Shutdown the executor
            executorService.shutdown();

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return new PageImpl<>(new ArrayList<AccountDTO>(), pageableRequest, 0);
        }
    }

    @Override
    public boolean isUsernameDuplicate(String username) {
        Account existingAccount = accountRepository.findByUsername(username);
        return existingAccount != null;
    }


    @Override
    public void exportExcelAccounts(HttpServletResponse response, QueryDTO queryDTO, String fieldSort, String direction) {

        try {
            ExecutorService executorService = Executors.newFixedThreadPool(5); // Maximum number of threads
            executorService.execute(() -> {
                transactionTemplate.execute(status -> {
                    try {
                        Pageable pageable = null;
                        Sort sort = null;
                        if ("desc".equalsIgnoreCase(direction)) {
                            sort = Sort.by(Sort.Direction.DESC, fieldSort);
                        } else {
                            sort = Sort.by(Sort.Direction.ASC, fieldSort);
                        }

                        pageable = PageRequest.of(0, ROW_TABLE_FILE, sort);
                        Page<AccountDTO> accountPage = accountGetByPage(pageable, queryDTO);

                        List<AccountDTO> dataList = modelMapper.map(accountPage.getContent(), new TypeToken<List<AccountDTO>>() {
                        }.getType());

                        String fileName = FileUtils.exportFileName("ACCOUNT_EXPORT_", ".xlsx");

                        // Create an Excel workbook and sheet
                        Workbook workbook = new XSSFWorkbook();
                        Sheet sheet = workbook.createSheet("Account List");

//            // Create a header row
//            Row headerRow = sheet.createRow(0);
//            headerRow.createCell(0).setCellValue("STT");
//            headerRow.createCell(1).setCellValue("Username");
//            headerRow.createCell(2).setCellValue("Email");
//            headerRow.createCell(3).setCellValue("Phone");
//            headerRow.createCell(4).setCellValue("Fullname");
//            headerRow.createCell(5).setCellValue("Birthday");
//            headerRow.createCell(6).setCellValue("Gender");
//            // Add more headers as needed

//            Apply style
                        CellStyle headerCellStyle = FileUtils.createHeaderCellExcel(workbook);

                        Row headerRow = sheet.createRow(0);

                        String[] headers = {"STT", "Username", "Email", "Phone", "Full Name", "Birthday", "Gender"};
                        for (int i = 0; i < headers.length; i++) {
                            Cell cell = headerRow.createCell(i);
                            cell.setCellValue(headers[i]);
                            cell.setCellStyle(headerCellStyle); // Apply default header cell style
                        }

                        CellStyle dateCellStyle = workbook.createCellStyle();
                        CreationHelper createHelper = workbook.getCreationHelper();
                        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

                        // Create data rows
                        int rowCount = 1;
                        for (AccountDTO data : dataList) {

                            Row dataRow = sheet.createRow(rowCount++);

                            dataRow.createCell(0).setCellValue(rowCount - 1);
                            dataRow.createCell(1).setCellValue(data.getUsername());
                            dataRow.createCell(2).setCellValue(data.getEmail());
                            dataRow.createCell(3).setCellValue(data.getPhone());
                            dataRow.createCell(4).setCellValue(data.getFullName());

                            // Apply date cell style
                            Cell birthdayCell = dataRow.createCell(5);
                            birthdayCell.setCellValue(data.getBirthday());
                            birthdayCell.setCellStyle(dateCellStyle);

                            dataRow.createCell(6).setCellValue(data.getGender());

                            // Add more fields as needed
                        }

                        for (int colIndex = 0; colIndex < headerRow.getLastCellNum(); colIndex++) {
                            sheet.autoSizeColumn(colIndex);
                        }

                        // Set content type and headers for the response
                        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

                        // Write the workbook to the response output stream
                        workbook.write(response.getOutputStream());

                        // Close the workbook
                        workbook.close();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                });
            });
            // Send tasks to the executor for processing
            executorService.shutdown();

            // Wait for all threads to complete
//            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

            // Continue processing after all threads are completed

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exportPDFAccounts(HttpServletResponse response, QueryDTO queryDTO, String fieldSort, String direction) {
        try {
            ExecutorService executorService = Executors.newFixedThreadPool(5);
            executorService.execute(() -> {
                transactionTemplate.execute(status -> {
                    Document document = new Document();

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try {

                        // Prepare your data for export
                        Pageable pageable = null;
                        Sort sort = null;
                        if ("desc".equalsIgnoreCase(direction)) {
                            sort = Sort.by(Sort.Direction.DESC, fieldSort);
                        } else {
                            sort = Sort.by(Sort.Direction.ASC, fieldSort);
                        }

                        pageable = PageRequest.of(0, ROW_TABLE_FILE, sort);
                        Page<AccountDTO> accountPage = accountGetByPage(pageable, queryDTO);

                        List<AccountDTO> dataList = modelMapper.map(accountPage.getContent(), new TypeToken<List<AccountDTO>>() {
                        }.getType());
                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

                        String fileName = FileUtils.exportFileName("ACCOUNT_EXPORT_", ".pdf");

                        PdfWriter.getInstance(document, baos);
                        document.open();

//            PdfPTable table = new PdfPTable(7);
//            Font headerFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK); // Customize header font
//
//            // Add table headers
//            table.addCell("STT");
//            table.addCell("Email");
//            table.addCell("Username");
//            table.addCell("Phone");
//            table.addCell("Full Name");
//            table.addCell("Birthday");
//            table.addCell("Gender");
//            // Create data rows
                        float[] columnWidths = {45f, 100f, 100f, 80f, 110f, 80f, 75f}; // Define column widths in points

                        PdfPTable table = new PdfPTable(columnWidths);
                        table.setWidthPercentage(100);

                        Font headerFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
                        table.addCell(FileUtils.createHeaderCellPDF("STT", headerFont));
                        table.addCell(FileUtils.createHeaderCellPDF("Email", headerFont));
                        table.addCell(FileUtils.createHeaderCellPDF("Username", headerFont));
                        table.addCell(FileUtils.createHeaderCellPDF("Phone", headerFont));
                        table.addCell(FileUtils.createHeaderCellPDF("Full Name", headerFont));
                        table.addCell(FileUtils.createHeaderCellPDF("Birthday", headerFont));
                        table.addCell(FileUtils.createHeaderCellPDF("Gender", headerFont));

                        int rowCount = 1;
                        for (AccountDTO account : dataList) {
                            table.addCell(String.valueOf(rowCount++));
                            table.addCell(account.getEmail());
                            table.addCell(account.getUsername());
                            table.addCell(account.getPhone());
                            table.addCell(account.getFullName());
                            if (account.getBirthday() == null) {
                                table.addCell("");

                            } else {
                                table.addCell(dateFormat.format(account.getBirthday()));
                            }
                            table.addCell(account.getGender());
                        }

                        document.add(table);

                        document.close();

                        byte[] pdfBytes = baos.toByteArray();
                        response.setContentLength(pdfBytes.length);
                        response.setContentType("application/pdf");
                        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);


                        OutputStream os = response.getOutputStream();
                        os.write(pdfBytes);
                        os.flush();
                        os.close();
                    } catch (DocumentException | IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                });
            });

            // Send tasks to the executor for processing
            executorService.shutdown();

            // Wait for all threads to complete
//            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

            // Continue processing after all threads are completed

        } catch (Exception ignored) {

        }

    }

    @Override
    public void exportDocAccounts(HttpServletResponse response, QueryDTO queryDTO, String fieldSort, String direction) {

        try {
            ExecutorService executorService = Executors.newFixedThreadPool(5);
            executorService.execute(() -> {
                transactionTemplate.execute(status -> {
                    try {
                        Pageable pageable = null;
                        Sort sort = null;
                        if ("desc".equalsIgnoreCase(direction)) {
                            sort = Sort.by(Sort.Direction.DESC, fieldSort);
                        } else {
                            sort = Sort.by(Sort.Direction.ASC, fieldSort);
                        }

                        pageable = PageRequest.of(0, ROW_TABLE_FILE, sort);
                        Page<AccountDTO> accountPage = accountGetByPage(pageable, queryDTO);

                        List<AccountDTO> dataList = modelMapper.map(accountPage.getContent(), new TypeToken<List<AccountDTO>>() {
                        }.getType());
                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

                        String fileName = FileUtils.exportFileName("ACCOUNT_EXPORT_", ".docx");

                        XWPFDocument document = new XWPFDocument();
                        OutputStream out = response.getOutputStream();

                        XWPFTable table = document.createTable(dataList.size() + 1, 7);

                        XWPFTableRow headerRow = table.getRow(0);
                        String[] headers = {"STT", "Email", "Username", "Phone", "Full Name", "Birthday", "Gender"};
                        for (int i = 0; i < headers.length; i++) {
                            XWPFTableCell headerCell = headerRow.getCell(i);
                            headerCell.setText(headers[i]);
                        }

                        for (int i = 0; i < dataList.size(); i++) {
                            AccountDTO account = dataList.get(i);
                            XWPFTableRow row = table.getRow(i + 1);

                            // Add STT
                            row.getCell(0).setText(String.valueOf(i + 1));

                            row.getCell(1).setText(account.getEmail());
                            row.getCell(2).setText(account.getUsername());
                            row.getCell(3).setText(account.getPhone());
                            row.getCell(4).setText(account.getFullName());
                            if (account.getBirthday() == null) {
                                row.getCell(5).setText("");

                            } else {
                                row.getCell(5).setText(dateFormat.format(account.getBirthday()));
                            }
                            row.getCell(6).setText(account.getGender());
                        }
                        response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

                        document.write(out);
                        out.close();
                    } catch (Exception ignored) {

                    }
                    return null;
                });
            });
            executorService.shutdown();

//            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean saveAccountsFromExcel(List<Account> accountList) {
        try {
            for (Account account : accountList) {
                createAccount(account);
            }
//            Iterable<Account> accountIterable = modelMapper.map(accountList, new TypeToken<Iterable<Account>>() {
//            }.getType());
//            accountRepository.save(accountIterable);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public long findMaxId() {
        return accountRepository.findMaxId();
    }

}
