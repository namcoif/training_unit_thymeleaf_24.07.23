package com.unit.usermanagement.service.impl;

import com.unit.usermanagement.entity.User;
import com.unit.usermanagement.repository.UserRepository;
import com.unit.usermanagement.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(transactionManager = "transactionManager")
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Iterable<User> userGetAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> findByUsernameMaxLength(int len, int size, Pageable pageable) {
        Page<User> userPage = userRepository.findByUsernameMaxLength(len, size, pageable);
        userPage.getContent();
        return userPage;
    }


}
