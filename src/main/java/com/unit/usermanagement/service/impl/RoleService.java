package com.unit.usermanagement.service.impl;

import com.unit.usermanagement.repository.RoleRepository;
import com.unit.usermanagement.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(transactionManager = "transactionManager")
public class RoleService implements IRoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void createRole(Long userId) {
        roleRepository.createRole(userId);
    }
}
