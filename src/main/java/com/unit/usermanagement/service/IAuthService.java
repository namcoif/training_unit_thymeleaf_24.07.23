package com.unit.usermanagement.service;

import com.unit.usermanagement.dto.LoginDTO;
import com.unit.usermanagement.entity.Account;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface IAuthService {
    Account login(LoginDTO loginDTO);

    CompletableFuture<Map<String, String>> googleCallBack(String code, String typeWeb);

    String googleAuth(String typeWeb);

    void logout();
}
