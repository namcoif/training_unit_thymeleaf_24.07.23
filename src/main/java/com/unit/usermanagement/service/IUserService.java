package com.unit.usermanagement.service;

import com.unit.usermanagement.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IUserService {
    Iterable<User> userGetAll();

    Page<User> findByUsernameMaxLength(int len, int size, Pageable pageable);

}
