package com.unit.usermanagement.entity;

import com.miragesql.miragesql.annotation.Column;
import com.miragesql.miragesql.annotation.PrimaryKey;
import com.miragesql.miragesql.annotation.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@ToString
@Table(name = "users")
@Data
@AllArgsConstructor
public class User {

    @Id
    @PrimaryKey(generationType = PrimaryKey.GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

}