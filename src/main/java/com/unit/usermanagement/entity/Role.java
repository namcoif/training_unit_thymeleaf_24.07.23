package com.unit.usermanagement.entity;

import com.miragesql.miragesql.annotation.Column;
import com.miragesql.miragesql.annotation.PrimaryKey;
import com.miragesql.miragesql.annotation.Table;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@ToString
@Table(name = "role")
@Data
public class Role {
    @Id
    @PrimaryKey(generationType = PrimaryKey.GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "role")
    private RoleEnum role;

    public enum RoleEnum {
        ADMIN, USER;

        public static RoleEnum toEnum(String role) {
            for (RoleEnum roleEnum : RoleEnum.values()) {
                if (roleEnum.toString().equals(role))
                    return roleEnum;
            }
            return null;
        }
    }
}
