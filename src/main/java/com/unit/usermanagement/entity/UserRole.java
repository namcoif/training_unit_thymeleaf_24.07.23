package vn.unit.jointable.entity;

import com.miragesql.miragesql.annotation.Column;
import com.miragesql.miragesql.annotation.Table;
import lombok.Data;
import lombok.ToString;

@ToString
@Table(name = "user_role")
@Data
public class UserRole {

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "role_id")
    private Integer roleId;
}
