package com.unit.usermanagement.entity;

import com.miragesql.miragesql.annotation.Column;
import com.miragesql.miragesql.annotation.PrimaryKey;
import com.miragesql.miragesql.annotation.Table;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.util.Date;


@ToString
@Table(name = "account")
@Data
public class Account {

    @Id
    @PrimaryKey(generationType = PrimaryKey.GenerationType.IDENTITY)
    @Column(name = "id")
    @Size(max = 20)
    private Long id;

    @Column(name = "username")
    @Size(max = 100, message = "Username length must be less than 100 characters!")
    private String username;

    @Column(name = "password")
    @Size(max = 255, message = "Password length must be less than 255 characters!")
    private String password;

    @Column(name = "account_type")
    @Size(max = 30)
    private String accountType;

    @Column(name = "fullname")
    @Size(max = 255, message = "Full name length must be less than 255 characters!")
    private String fullName;

    @Column(name = "email")
    @Size(max = 100, message = "Email length must be less than 100 characters!")
    private String email;

    @Column(name = "birthday")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @Column(name = "phone")
    @Size(max = 30, message = "Phone length must be less than 30 characters!")
    private String phone;

    @Column(name = "gender")
    @Size(max = 30)
    private String gender;

    @Column(name = "actived")
    @Size(max = 1)
    private boolean actived;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "created_id")
    @Size(max = 20)
    private Long createdId;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Column(name = "updated_id")
    @Size(max = 20)
    private Long updatedId;

    @Column(name = "deleted_date")
    private Date deletedDate;

    @Column(name = "deleted_id")
    @Size(max = 20)
    private Long deletedId;

    @Column(name = "delete_flag")
    private boolean deletedFlag;

}