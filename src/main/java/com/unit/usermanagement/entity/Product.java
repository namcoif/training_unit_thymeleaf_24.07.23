package com.unit.usermanagement.entity;

import com.miragesql.miragesql.annotation.Column;
import com.miragesql.miragesql.annotation.PrimaryKey;
import com.miragesql.miragesql.annotation.Table;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Size;
import java.util.Date;

@ToString
@Table(name = "product")
@Data
public class Product {
    @Id
    @PrimaryKey(generationType = PrimaryKey.GenerationType.IDENTITY)
    @Column(name = "id")
    @Size(max = 20)
    private Long id;

    @Column(name = "name")
    @Size(max = 100, message = "Username length must be less than 100 characters!")
    private String name;

    @Id
    @Column(name = "description")
    @Size(max = 1000, message = "Product description must be less than 1000 characters!")
    private String description;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "created_id")
    @Size(max = 20)
    private Long createdId;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Column(name = "updated_id")
    @Size(max = 20)
    private Long updatedId;

    @Column(name = "deleted_date")
    private Date deletedDate;

    @Column(name = "deleted_id")
    @Size(max = 20)
    private Long deletedId;

    @Column(name = "delete_flag")
    private boolean deletedFlag;
}