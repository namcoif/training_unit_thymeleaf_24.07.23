BEGIN
    DECLARE @roleId INT
    DECLARE @userId BIGINT

    /*SET @userId = user_id*/

    SELECT @roleId = id
    FROM role r
    WHERE r.role = 'USER'

    INSERT INTO user_role (user_id, role_id)
    VALUES (@userId, @roleId)
END
