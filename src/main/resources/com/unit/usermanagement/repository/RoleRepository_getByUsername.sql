WITH CTE AS(
    SELECT a.id AS user_id
    FROM account a
    WHERE a.username= /*username*/
)
SELECT r.id, r.role, CTE.user_id
FROM role r
         JOIN user_role ur ON r.id= ur.role_id
         JOIN CTE ON ur.user_id = CTE.user_id;