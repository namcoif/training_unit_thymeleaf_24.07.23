-- Get total elements
DECLARE @totalElements INT

SELECT @totalElements = COUNT(*)
FROM account a
WHERE 1 = 1 AND a.delete_flag = 0
    /*IF queryDTO.username!=null*/
  AND a.username LIKE '%' + /*queryDTO.username*/ + '%'
    /*END*/
    /*IF queryDTO.fullName!=null*/
  AND a.fullname LIKE '%' + /*queryDTO.fullName*/ + '%'
    /*END*/
    /*IF queryDTO.accountType=='ADMIN'*/
  AND a.account_type LIKE '%ADMIN%'
    /*END*/
    /*IF queryDTO.accountType=='USER'*/
  AND a.account_type LIKE '%USER%'
    /*END*/
    /*IF queryDTO.birthday!=null*/
  AND a.birthday = /*queryDTO.birthday*/
    /*END*/
    /*IF queryDTO.actived=='false'*/
  AND a.actived = 0
    /*END*/
    /*IF queryDTO.actived=='true'*/
  AND a.actived = 1
    /*END*/

-- Get total pages
DECLARE @pageSize INT = /*size*/
DECLARE @totalPages INT

SELECT @totalPages = CEILING(CAST(@totalElements AS FLOAT) / @pageSize);

-- Get by pages
WITH CTE AS (
    SELECT a.[id],
--            a.[account_type],
           a.[actived],
           a.[birthday],
           a.[created_date],
--            a.[created_id],
--            a.[deleted_date],
--            a.[delete_flag],
           a.[deleted_id],
           a.[email],
           a.[fullname],
           a.[gender],
--            a.[password],
           a.[phone],
           a.[updated_date],
           a.[updated_id],
           a.[username],
           creater.[username] AS creater,
           updater.[username] AS updater
    FROM account a
    LEFT JOIN  account creater ON a.[created_id]=creater.[id]
    LEFT JOIN  account updater ON a.[updated_id]=updater.[id]

    WHERE 1 = 1 AND a.delete_flag = 0
        /*IF queryDTO.username!=null*/
            AND a.username LIKE '%' + /*queryDTO.username*/ + '%'
        /*END*/
        /*IF queryDTO.fullName!=null*/
            AND a.fullname LIKE '%' + /*queryDTO.fullName*/ + '%'
        /*END*/
        /*IF queryDTO.accountType=='ADMIN'*/
            AND a.account_type LIKE '%ADMIn%'
        /*END*/
        /*IF queryDTO.accountType=='USER'*/
             AND a.account_type LIKE '%USER%'
        /*END*/
        /*IF queryDTO.birthday!=null*/
            AND a.birthday = /*queryDTO.birthday*/
        /*END*/
        /*IF queryDTO.actived=='false'*/
            AND a.actived = 0
        /*END*/
        /*IF queryDTO.actived=='true'*/
            AND a.actived = 1
        /*END*/

    )
SELECT *,
       @totalElements AS totalElements,
       @totalPages AS totalPages
FROM CTE
ORDER BY COALESCE(CTE.updated_date, CTE.created_date) DESC
    /*IF offset != null*/
        OFFSET /*offset*/1 ROWS FETCH NEXT  /*size*/1 ROWS ONLY
    /*END*/