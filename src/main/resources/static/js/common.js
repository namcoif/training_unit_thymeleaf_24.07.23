var queriedUsername;
var queriedFullName;
var queriedBirthday;
var queriedAccountType;
var queriedActived;

function getQueryFromLocalStorage() {
    // Lấy giá trị từ localStorage
    queriedUsername = localStorage.getItem("queriedUsername") != null ? localStorage.getItem("queriedUsername") : "";
    queriedFullName = localStorage.getItem("queriedFullName") != null ? localStorage.getItem("queriedFullName") : "";
    queriedBirthday = localStorage.getItem("queriedBirthday") != null ? localStorage.getItem("queriedBirthday") : "";
    queriedAccountType = localStorage.getItem("queriedAccountType") != null ? localStorage.getItem("queriedAccountType") : "";
    queriedActived = localStorage.getItem("queriedActived") != null ? localStorage.getItem("queriedActived") : "";
}

function validatorMaxLength() {
    var fieldsToValidate = document.querySelectorAll(".validate-field");
    fieldsToValidate.forEach(function (field) {
        const container = field.closest('.feild-input');
        const errorElement = document.createElement("div");
        errorElement.className = "alert-danger"
        container.appendChild(errorElement);

        field.addEventListener('input', function () {
            const maxLength = parseInt(field.getAttribute('maxlength'));
            if (this.value.length >= maxLength) {
                errorElement.innerText = `Must not exceed ${maxLength} characters.`;
                errorElement.style.display = 'block';
                setTimeout(function () {
                    errorElement.style.display = 'none';
                }, 3000); // 10000 milliseconds = 10 seconds
            } else {
                errorElement.innerText = '';
            }
        });
    });
}