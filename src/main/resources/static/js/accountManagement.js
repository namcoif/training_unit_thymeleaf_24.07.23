function openSaveUser() {
    var dialogCreateUser = document.getElementById('dialogCreateUser');
    dialogCreateUser.style.display = 'block';
}

function closeSaveUser() {
    var dialogCreateUser = document.getElementById('dialogCreateUser');
    dialogCreateUser.style.display = 'none';
    window.location.href = "/mvc/admin/account-management/paging";

}

function openDeleteUser() {
    var dialogDeleteUser = document.getElementById('dialogDeleteUser');

    dialogDeleteUser.style.display = 'block';
}

function closeDeleteUser() {
    var dialogDeleteUser = document.getElementById('dialogDeleteUser');
    dialogDeleteUser.style.display = 'none';
    window.location.href = "/mvc/admin/account-management/paging";
}

function clearSearchForm() {
    var form = document.getElementById('search-input');

    document.getElementById('usernameS').value = '';
    document.getElementById('fullNameS').value = '';
    document.getElementById('birthdayS').value = '';
    document.getElementById('accountTypeS').value = 'all';
    document.getElementById('activedS').value = 'all';

    form.submit();
    localStorage.setItem("queriedUsername", "");
    localStorage.setItem("queriedFullName", "");
    localStorage.setItem("queriedBirthday", "");
    localStorage.setItem("queriedAccountType", "");
    localStorage.setItem("queriedActived", "");
}

function getQuerySearchAccounts() {
    document.getElementById("search-input").addEventListener("submit", function (event) {

        var username = document.getElementById("usernameS").value;
        var fullName = document.getElementById("fullNameS").value;
        var birthday = document.getElementById("birthdayS").value;
        var accountType = document.getElementById("accountTypeS").value;
        var actived = document.getElementById("activedS").value;

        localStorage.setItem("queriedUsername", username);
        localStorage.setItem("queriedFullName", fullName);
        localStorage.setItem("queriedBirthday", birthday);
        localStorage.setItem("queriedAccountType", accountType);
        localStorage.setItem("queriedActived", actived);
    });
}

function updateParamQuery() {
    var linkAccountList = document.querySelectorAll(".link-account-list");

    // Cập nhật tham số của các liên kết
    for (var i = 0; i < linkAccountList.length; i++) {
        var link = linkAccountList[i];
        link.href = link.href + "&username=" + encodeURIComponent(queriedUsername) +
            "&fullName=" + encodeURIComponent(queriedFullName) +
            "&birthday=" + encodeURIComponent(queriedBirthday) +
            "&accountType=" + encodeURIComponent(queriedAccountType) +
            "&actived=" + encodeURIComponent(queriedActived);
    }

    // Lấy danh sách các liên kết có lớp "update-link"
    var linkExportAccounts = document.querySelectorAll(".link-export-accounts");

    linkExportAccounts.forEach(function (link) {
        link.href = link.href + "?username=" + encodeURIComponent(queriedUsername) +
            "&fullName=" + encodeURIComponent(queriedFullName) +
            "&birthday=" + encodeURIComponent(queriedBirthday) +
            "&accountType=" + encodeURIComponent(queriedAccountType) +
            "&actived=" + encodeURIComponent(queriedActived);
    });

    // var exportExcelAccounts = document.getElementById("link-export-excel-accounts");
    // exportExcelAccounts.href = exportExcelAccounts.href + "?username=" + encodeURIComponent(queriedUsername) +
    //     "&fullName=" + encodeURIComponent(queriedFullName) +
    //     "&birthday=" + encodeURIComponent(queriedBirthday) +
    //     "&accountType=" + encodeURIComponent(queriedAccountType) +
    //     "&actived=" + encodeURIComponent(queriedActived);
}

function validateNumberInput(input) {
    var value = input.value;
    var isValid = /^\d+$/.test(value); // Kiểm tra nếu chuỗi chỉ chứa số
    if (!isValid) {
        alert("Please enter numbers only!");
        input.value = ''; // Xóa giá trị nếu không hợp lệ
    }
}
