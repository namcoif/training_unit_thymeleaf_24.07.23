function goBack() {
    var url = "/mvc/admin/account-management/paging" + "?username=" + encodeURIComponent(queriedUsername) +
        "&fullName=" + encodeURIComponent(queriedFullName) +
        "&birthday=" + encodeURIComponent(queriedBirthday) +
        "&accountType=" + encodeURIComponent(queriedAccountType) +
        "&actived=" + encodeURIComponent(queriedActived);
    window.location.href = url;
}